const button = document.querySelector('#button');
const login = document.querySelector('#login');
const pass = document.querySelector('#pass');
const output = document.querySelector('#output');


function check() {
	const loginValue = login.value;
	const passValue = pass.value;

	if (!loginValue && !passValue) {
		output.innerHTML = 'Вы не заполнили поля логин и пароль'
		login.style.backgroundColor = 'red';
		pass.style.backgroundColor = 'red';
	}

	if (loginValue == "admin" && passValue == "12345") {
		output.innerHTML = 'Вы авторизованы'
		output.style.color = 'green';
	}
}

button.addEventListener('click', check)